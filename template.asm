;====================================================================
; Processor		: ATmega8515
; Compiler		: AVRASM
;====================================================================

;====================================================================
; DEFINITIONS
;====================================================================

.include "m8515def.inc"
.def last_key_pressed = r14 ;register untuk menyimpan value dari keypad yang ditekan
.def temp = r16				; temporary register 0
.def temp_1 = r17 			;temporary register 1
.def game_value = r22 		;status game, 0x00 berarti kalah, 0xFF berarti menang, 0x0F berarti game masih berjalan
.def zh_pointer_pola = r23 	;zh_pointer_pola dan zl_pointer_pola untuk menyimpan alamat dari pola
.def zl_pointer_pola = r24 	;untuk melakukan tracking pola yang digunakan
.def timer_counter = r25 	;timer, jika timer_counter nilainya 0, maka set game_status menjadi 0x00

.org $00
rjmp MAIN
rjmp EXT_INT0 ;address $00 untuk interrupt INT0
rjmp EXT_INT1 ;address $01 untuk interrupt INT1
.org $07
rjmp ISR_TOV0 ;address $07 untuk interrupt timer0

;====================================================================
; BLOCK MAIN
; block ini berfungsi untuk menginisiasi semua yang diperlukan
; program agar dapat berjalan
;====================================================================

MAIN:
;init stack pointer diawal karena akan terus digunakan. ex: rcall
INIT_STACK:
	ldi temp, low(RAMEND)
	out SPL, temp
	ldi temp, high(RAMEND)
	out SPH, temp

;init pointer Z ke block data "message"
;kemudian outputkan data ke LCD

rcall INIT_MESSAGE_DATA		;int pointer Z
rcall INIT_LCD 				;output pertama "MISTERY GAME" di baris 1 LCD
adiw ZL, 1 					;pindah pointer ke data selanjutnya
rcall DELAY_02 				;lakukan delay agar tulisan dapat dilihat
rcall DELAY_02
rcall INIT_LCD				;output kedua "GAME START" di baris 1 LCD
adiw ZL, 1
rcall DELAY_02
rcall DELAY_02
rcall DELAY_02
rcall INIT_LCD				;output ketiga "TIME REMAINING :" di baris 1 LCD

rcall READ_DATA_POLA_INIT_POINTER	;init pointer Z ke block data pola
rcall READ_DATA_POLA				;set register yang telah ditentukan dengan pola yang sudah disediakan
adiw ZL, 1							;pindah pointer ke data selanjutnya

;simpan address dari data pola yang selanjutnya ke register zh_pointer_pola dan zl_pointer_pola
mov zh_pointer_pola, ZH		
mov zl_pointer_pola, ZL

;set timer_counter 60
;sehingga jika user belum memenangkan game dalam waktu 60 timer interrupt cycle
;maka game_value akan diset menjadi 0x00
ldi timer_counter, 60

rcall INIT_TIMER_DATA	;init pointer Z ke block data time
rcall LCD_TIMER			;output data pertama ke baris 2

rcall INIT_LED			;set led menjadi 0x00 (mati semua)

ldi game_value, 0x0F	;set game_value menjadi 0x0F, yang berarti game masih berjalan

rcall INIT_TIMER_INTERRUPT
rcall INIT_BUTTON_INTERUPT
rjmp GAME_START			

INIT_MESSAGE_DATA:
	ldi ZH, high(2*message)
	ldi ZL, low(2*message)
	ret

INIT_TIMER_DATA:
	ldi ZH, high(2*timer)
	ldi ZL, low(2*timer)
	ret

INIT_KEYPAD:
	ldi temp, 0x00
	mov last_key_pressed, temp
	ret

INIT_LED:
	ldi r19, 0
	ldi r20, 0
	ser temp
	out DDRA, temp
	out PORTA, r20
	ret

;set interrupt timer0 dengan Pck/1024
INIT_TIMER_INTERRUPT:
	ldi temp, 0b00000101
	out TCCR0, temp
	ldi temp, (1<<TOV0)
	out TIFR, temp
	ldi temp, (1<<TOIE0)
	out TIMSK, temp
	ret

;set interrupt button ext_int0 dan ext_int1, melakukan interrupt ketika falling edge
INIT_BUTTON_INTERUPT:
	ldi temp, 0x00
	out DDRD, temp
	out PORTD, temp

	ldi temp, 0b11000000
	out GICR, temp

	ldi temp, 0b00001010
	out MCUCR, temp

	sei
	ret

;===============================================================
; BAGIAN utama dari game
; masuk ke block FOREVER jika game_value sama dengan 0x00 atau 0xFF
; block KALAH untuk write data "-TIME OUT-" ke lcd, lalu jump ke block FOREVER
; block MENANG untuk write data "CONGRATULATION" ke lcd, lalu jump ke block FOREVER
; block GAME_START merupakan bagian untuk menerima input keypad lalu melakukan operasi yang diperlukan
;===============================================================

;loop forever, jika game_value menjadi 0x0F (dapat terjadi ketika interrupt), kembali ke block GAME_START
FOREVER:
	cpi game_value, 0x0F
	breq SCANNING_KEYPAD
	rjmp FOREVER

KALAH:
	ldi ZH, high(2*kalah_msg)
	ldi ZL, low(2*kalah_msg)

	rcall LOAD_DATA_TIMER

	rjmp FOREVER

MENANG:
	ldi ZH, high(2*menang_msg)
	ldi ZL, low(2*menang_msg)

	cbi PORTB, 1
	cbi PORTB, 2
	ldi temp, 0b11000000
	out PORTE, temp
	sbi PORTB, 0 ;set enable
	cbi PORTB, 0 ;clear enable
 
	rcall LOAD_DATA_TIMER

	rjmp FOREVER

GAME_START:

	SCANNING_KEYPAD:
		cpi game_value, 0xFF  ;jika game_value 0xFF branch ke block MENANG
		breq MENANG

		POWER_UP_ROW:
			; By powering all row lines, we can detect the column of pressed key
			ldi temp, 0b00001111
			out DDRC, temp ; Set row lines as output
			out PORTC, temp ; Powering row lines

		READ_COLUMN:
			rcall DELAY
			cpi game_value, 0x00 ;jika game_value 0x00 branch ke block KALAH
			breq KALAH
			in temp, PINC ; Read the column
			cpi temp, 0b00001111 ; If all column is low, no key is pressed
			brne POWER_UP_COLUMN

		FALLING_EDGE_TRIGGER:
			; When nothing is pressed, we simulate falling edge trigger
			; Which mean key is only processed when user release the button
			mov temp, last_key_pressed
			cpi temp, 0x00
			brne KEY_CHECK
			rjmp READ_COLUMN

		POWER_UP_COLUMN:
			; By powering all column lines, we can detect the column of pressed key
			ldi temp_1, 0b11110000
			out DDRC, temp_1 ; Set row lines as output
			out PORTC, temp_1 ; Powering row lines

		READ_ROW:
			rcall DELAY
			in temp_1, PINC ; Read the row
			cpi temp, 0b11110000 ; Failsafe for fast key press
			breq POWER_UP_ROW
			and temp, temp_1 ; Combining the two, we can get which button is pressed
			
			mov temp_1, last_key_pressed	; We are not going to process the button
			cpi temp_1, 0x00				; If we're not yet processing the previous button
			brne POWER_UP_ROW

			mov last_key_pressed, temp ; Save the button to a register to simulate falling edge trigger
			rjmp POWER_UP_ROW

	KEY_CHECK:
		; Clear last key pressed
		ldi temp_1, 0x00
		mov last_key_pressed, temp_1


		; If two last column is pressed, immediately goes back to scanning
		; We do not use those column
		sbrc temp, 4
		rjmp SCANNING_KEYPAD
		sbrc temp, 5
		rjmp SCANNING_KEYPAD

		cpi temp, 0b10001000
		breq POLA1
		cpi temp, 0b10000100
		breq POLA3
		cpi temp, 0b10000010
		breq POLA5
		cpi temp, 0b10000001
		breq POLA7
		cpi temp, 0b01001000
		breq POLA2
		cpi temp, 0b01000100
		breq POLA4
		cpi temp, 0b01000010
		breq POLA6
		cpi temp, 0b01000001
		breq POLA8
		rjmp SCANNING_KEYPAD

		POLA1:	
		mov r19, r5				;dapatkan value dari pola
		eor r20, r19			;lakukan XOR pola dengan value LED
		out PORTA, r20			;kemudian output hasilnya
		cpi r20, 0xFF			;jika r20 0xFF (LED menyala semua), maka set game_value menjadi 0xFF
		breq SET_GAME_VALUE_TO_FF
		rjmp SCANNING_KEYPAD	;jump ke block SCANNING_KEYPAD untuk membaca input keypad selanjutnya

		POLA2:	
		mov r19, r6
		eor r20, r19
		out PORTA, r20
		cpi r20, 0xFF
		breq SET_GAME_VALUE_TO_FF
		rjmp SCANNING_KEYPAD

		POLA3:	
		mov r19, r7
		eor r20, r19
		out PORTA, r20
		cpi r20, 0xFF
		breq SET_GAME_VALUE_TO_FF
		rjmp SCANNING_KEYPAD

		POLA4:	
		mov r19, r8
		eor r20, r19
		out PORTA, r20
		cpi r20, 0xFF
		breq SET_GAME_VALUE_TO_FF
		rjmp SCANNING_KEYPAD

		POLA5:	
		mov r19, r9
		eor r20, r19
		out PORTA, r20
		cpi r20, 0xFF
		breq SET_GAME_VALUE_TO_FF
		rjmp SCANNING_KEYPAD

		POLA6:	
		mov r19, r10
		eor r20, r19
		out PORTA, r20
		cpi r20, 0xFF
		breq SET_GAME_VALUE_TO_FF
		rjmp SCANNING_KEYPAD

		POLA7:	
		mov r19, r11
		eor r20, r19
		out PORTA, r20
		cpi r20, 0xFF
		breq SET_GAME_VALUE_TO_FF
		rjmp SCANNING_KEYPAD

		POLA8:	
		mov r19, r12
		eor r20, r19
		out PORTA, r20
		cpi r20, 0xFF
		breq SET_GAME_VALUE_TO_FF
		rjmp SCANNING_KEYPAD

SET_GAME_VALUE_TO_FF:
	ldi game_value, 0xFF
	rjmp SCANNING_KEYPAD

DELAY:
	nop
	ret

DELAY_01:
	    ldi  r18, 30
	    ldi  r19, 49
	L1: dec  r19
	    brne L1
	    dec  r18
	    brne L1
	    nop
	ret

DELAY_02:
	    ldi  r18, 208
	    ldi  r19, 202
	L2: dec  r19
	    brne L2
	    dec  r18
	    brne L2
	    nop
		ret

;===============================================================
; BAGIAN INTERRUPT
; terdapat tiga interrupt dalam program ini
; 1. ext_int0, berfungsi untuk melakukan reset program
; 2. ext_int1, berfungsi untuk untuk mereset led, timer lcd, dan merubah pola
; 3. isr_tov0, berfungsi sebagai timer dan melakukan write data time ke lcd
;===============================================================

;RESET
;melakukan fungsi yang hampir sama seperti block MAIN
;yang membedakan adalah r5-r12 diset menjadi 0
EXT_INT0:
	in temp, SREG ;simpan SREG sebelum dirubah oleh interrupt
	push temp
	
	rcall INIT_MESSAGE_DATA
	rcall INIT_LCD
	adiw ZL, 1
	rcall DELAY_02
	rcall DELAY_02
	rcall INIT_LED
	rcall INIT_LCD
	adiw ZL, 1
	rcall DELAY_02
	rcall DELAY_02
	rcall DELAY_02
	rcall INIT_LCD

	ldi game_value, 0x0F
	
	ldi temp, 0x00
	mov r5, temp
	mov r6, temp
	mov r7, temp
	mov r8, temp
	mov r9, temp
	mov r10, temp
	mov r11, temp
	mov r12, temp

	rcall READ_DATA_POLA_INIT_POINTER
	rcall READ_DATA_POLA
	adiw ZL, 1
	mov zh_pointer_pola, ZH
	mov zl_pointer_pola, ZL

	ldi timer_counter, 60

	rcall INIT_TIMER_DATA
	rcall LCD_TIMER
	
	pop temp
	out SREG, temp 	;kembalikan nilai SREG menjadi seperti sebelum terjadi interrupt
	
	reti

;CONTINUE
;continue hanya "benar-benar bekerja" ketika game_valuenya 0xFF (sudah menang)
;melakukan clear lcd, led. kemudian siapkan game dengan pola yang baru
EXT_INT1:
	in temp, SREG
	push temp

	cpi game_value, 0xFF
	brne MASIH_MAIN

	;set value dari pola yang sekarang menjadi 0
	;agar bisa diset dengan pola yang baru
	ldi temp, 0x00
	mov r5, temp
	mov r6, temp
	mov r7, temp
	mov r8, temp
	mov r9, temp
	mov r10, temp
	mov r11, temp
	mov r12, temp

	;load address pola yang baru dari zh_pointer_pola dan zl_pointer_pola ke pointer Z
	mov ZH, zh_pointer_pola
	mov ZL, zl_pointer_pola

	;reset timer
	ldi timer_counter, 60

	lpm

	;jika sudah mencapai akhir pola, maka akan branch ke AKHIR_POLA yang berfungsi
	;set pola dari pola yang pertama
	mov temp, r0
	cpi temp, 0xFF
	breq AKHIR_POLA

	;set r5-r12 dengan pola selanjutnya
	rcall READ_DATA_POLA
	adiw ZL, 1
	mov zh_pointer_pola, ZH
	mov zl_pointer_pola, ZL
	
	;reset game_value
	ldi game_value, 0x0F

	;set pointer Z ke block data continue
	ldi ZL, low(2*continue)
	ldi ZH, high(2*continue)

	;clear lcd dan led, lalu write data
	rcall CLR_LCD
	rcall INIT_LED
	rcall INIT_LCD

	adiw ZL, 1
	rcall DELAY_02
	rcall DELAY_02
	rcall DELAY_02
	rcall INIT_LCD
	
	;init time lcd
	;kemudian lakukan write data
	rcall INIT_TIMER_DATA
	rcall LCD_TIMER
	
	pop temp
	out SREG, temp

	reti

MASIH_MAIN:
	pop temp
	out SREG, temp
	reti

AKHIR_POLA:
	rcall READ_DATA_POLA_INIT_POINTER
	rcall READ_DATA_POLA
	adiw ZL, 1
	mov zh_pointer_pola, ZH
	mov zl_pointer_pola, ZL
	
	ldi game_value, 0x0F

	ldi ZL, low(2*continue)
	ldi ZH, high(2*continue)
	
	rcall CLR_LCD
	rcall INIT_LED
	rcall INIT_LCD

	adiw ZL, 1
	rcall DELAY_02
	rcall DELAY_02
	rcall DELAY_02
	rcall INIT_LCD
	
	rcall INIT_TIMER_DATA
	rcall LCD_TIMER
	
	pop temp
	out SREG, temp
	reti
	
;setiap terjadi interrupt timer, maka timer_counter akan di decrement, dan timer lcd akan diganti dengan value yang baru
;jika timer_counter sudah 0x00, maka game_value akan diset ke 0x00 (kalah)
ISR_TOV0:
	in temp, SREG
	push temp

	cpi game_value, 0x0F
	brne KEMBALI

	cpi timer_counter, 0x00
	breq SET_GAME_VALUE_TO_00
	dec timer_counter

	lpm

	mov temp, r0
	cpi temp, 0xFF
	breq KEMBALI
	adiw ZL, 1
	rcall LCD_TIMER
	
	pop temp
	out SREG, temp
	
	reti

SET_GAME_VALUE_TO_00:
	ldi game_value, 0x00
	
	pop temp
	out SREG, temp
	
	reti

KEMBALI:
	pop temp
	out SREG, temp
	
	reti

;===============================================================
; BAGIAN LCD
; PORTB sebagai input
; PORTE sebagai output
; EN bit 0
; RS bit 1
; R/W bit 2
;===============================================================

;init_lcd melakukan clear lcd kemudian melakukan write data ke lcd
INIT_LCD:
	rcall CLR_LCD
	rcall LOAD_DATA
	ret

;lcd timer melakukan set pointer lcd ke address $40
;yaitu baris kedua kolom pertama, lalu melakukan write data ke lcd
LCD_TIMER:
	cbi PORTB, 1
	cbi PORTB, 2
	ldi temp, 0b11000000
	out PORTE, temp
	sbi PORTB, 0 		;set enable
	cbi PORTB, 0 		;clear enable

	rcall LOAD_DATA_TIMER
	ret

;load data untuk timer
;perbedaannya dengan load data yang biasa adalah tidak melakukan clear lcd
LOAD_DATA_TIMER:
	lpm 					;load karakter di pointer Z ke r0

	adiw ZL, 1 				;add 1 ke Z low
	tst r0 					;cek apakah r0 <= 0, jika true maka SREG Z = 1
	breq END_LCD_IF00 		;branch ke END_LCD_IF00 jika Z = 1

	mov temp, r0 			;pindahkan nilai r0 ke temp
	cpi temp, 0xFF 			;cek apakah r0 == FF, jika true maka SREG Z = 1
	breq END_LCD_IFFF_TIMER ;branch ke END_LCD_IFFF_TIMER jika Z = 1
	
	mov temp_1, r0 			;pindah data dari r0 ke variable A
	rcall WRITE_DATA 		;jalankan block program untuk mencetak data ke LCD
	rjmp LOAD_DATA_TIMER 	;loop sampai block program berakhir

END_LCD_IFFF_TIMER:
	ret

;load data karakter pe karakter
;jika masih ada karakter ( > 0 || < 0xFF), maka akan terus dicetak ke LCD
LOAD_DATA:
	lpm 				;load karakter di pointer Z ke r0

	adiw ZL, 1 			;add 1 ke Z low
	tst r0 				;cek apakah r0 <= 0, jika true maka SREG Z = 1
	breq END_LCD_IF00 	;branch ke END_LCD_IF00 jika Z = 1

	mov temp, r0 		;pindahkan nilai r0 ke temp
	cpi temp, 0xFF 		;cek apakah r0 == FF, jika true maka SREG Z = 1
	breq END_LCD_IFFF 	;branch ke END_LCD_IFFF jika Z = 1
	
	mov temp_1, r0 		;pindah data dari r0 ke variable A
	rcall WRITE_DATA 	;jalankan block program untuk mencetak data ke LCD
	rcall DELAY_01
	rjmp LOAD_DATA 		;loop sampai block program berakhir

END_LCD_IF00:
	ret

END_LCD_IFFF:
	rcall INIT_LCD ;jika pointer sudah sampai end of data, maka akan direset
	ret

;;melakukan clear lcd. set RS, clear R/W, dan kemudian output data yang telah disimpan
WRITE_DATA:
	sbi PORTB, 1
	cbi PORTB, 2
	out PORTE, temp_1
	sbi PORTB, 0
	cbi PORTB, 0
	ret

;melakukan clear lcd. clear RS dan R/W, kemudian output 0x01
CLR_LCD:
	cbi PORTB, 1
	cbi PORTB, 2
	ldi temp, 0x01
	out PORTE, temp
	sbi PORTB, 0 ;set enable
	cbi PORTB, 0 ;clear enable
	ret ;kembali ke address yang melakukan rcall

;===============================================================
; BAGIAN read data pola
; membaca data dari block data pola
; kemudian memasukkannya secara terurut ke r5-r12
;===============================================================

READ_DATA_POLA_INIT_POINTER:
	ldi ZH, high(2*pola)
	ldi ZL, low(2*pola)
	ret

;load data pola ke register r5-r12 secara terurut
READ_DATA_POLA:
	lpm

	tst r0
	breq ADIW_END_LOAD

	tst r5
	breq LOAD_POLA1

	tst r6
	breq LOAD_POLA2

	tst r7
	breq LOAD_POLA3

	tst r8
	breq LOAD_POLA4

	tst r9
	breq LOAD_POLA5

	tst r10
	breq LOAD_POLA6

	tst r11
	breq LOAD_POLA7

	tst r12
	breq LOAD_POLA8
	
ADIW_READ_DATA:
	adiw ZL, 1 ; move pointer to next data in array
	rjmp READ_DATA_POLA

LOAD_POLA1:
	mov r5, r0
	rjmp ADIW_READ_DATA
LOAD_POLA2:
	mov r6, r0
	rjmp ADIW_READ_DATA
LOAD_POLA3:
	mov r7, r0
	rjmp ADIW_READ_DATA
LOAD_POLA4:
	mov r8, r0
	rjmp ADIW_READ_DATA
LOAD_POLA5:
	mov r9, r0
	rjmp ADIW_READ_DATA
LOAD_POLA6:
	mov r10, r0
	rjmp ADIW_READ_DATA
LOAD_POLA7:
	mov r11, r0
	rjmp ADIW_READ_DATA
LOAD_POLA8:
	mov r12, r0
	rjmp ADIW_READ_DATA

ADIW_END_LOAD:
	adiw ZL,1
	ret
	
;===============================================================
; BAGIAN BLOCK DATA
;===============================================================

pola:
.db 0b11111110, 0b01111111, 0b11100110, 0b01110011, 0b10011110, 0b10110000, 0b00101001, 0b01100110, 0
.db 0b10110000, 0b00101001, 0b01100110, 0b11111110, 0b01111111, 0b11100110, 0b01110011, 0b10011110, 0
.db 0b11111110, 0b01111111, 0b11100110, 0b10110000, 0b00101001, 0b01100110, 0b01110011, 0b10011110, 0
.db 0b11111110, 0b01111111, 0b11100110, 0b01110011, 0b10011110, 0b10110000, 0b00101001, 0b01100110, 0
.db 0b10110000, 0b00101001, 0b01100110, 0b11111110, 0b01111111, 0b11100110, 0b01110011, 0b10011110, 0
.db 0xFF, 0xFF

message:
.db "MISTERY GAME", 0

continue:
.db "GAME START", 0
.db "TIME REMAINING :", 0

kalah_msg:
.db " -TIME OUT-", 0

menang_msg:
.db "CONGRATULATION", 0

timer:
.db "60" , 0
.db "59" , 0
.db "58" , 0
.db "57" , 0
.db "56" , 0
.db "55" , 0
.db "54" , 0
.db "53" , 0
.db "52" , 0
.db "51" , 0
.db "50" , 0
.db "49" , 0
.db "48" , 0
.db "47" , 0
.db "46" , 0
.db "45" , 0
.db "44" , 0
.db "43" , 0
.db "42" , 0
.db "41" , 0
.db "40" , 0
.db "39" , 0
.db "38" , 0
.db "37" , 0
.db "36" , 0
.db "35" , 0
.db "34" , 0
.db "33" , 0
.db "32" , 0
.db "31" , 0
.db "30" , 0
.db "29" , 0
.db "28" , 0
.db "27" , 0
.db "26" , 0
.db "25" , 0
.db "24" , 0
.db "23" , 0
.db "22" , 0
.db "21" , 0
.db "20" , 0
.db "19" , 0
.db "18" , 0
.db "17" , 0
.db "16" , 0
.db "15" , 0
.db "14" , 0
.db "13" , 0
.db "12" , 0
.db "11" , 0
.db "10" , 0
.db "09" , 0
.db "08" , 0
.db "07" , 0
.db "06" , 0
.db "05" , 0
.db "04" , 0
.db "03" , 0
.db "02" , 0
.db "01" , 0
.db "00" , 0
