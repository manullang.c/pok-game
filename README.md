**PETUNJUK MENJALANKAN APLIKASI "MISTERY GAME"**

1. Load file TugasAkhir.asm ke AVR Studio
2. Build program yang telah di load tadi
3. Buka hapsim dan load TA.xml
4. Pastikan hapsim telah terhubung dengan AVR, lalu run program di AVR
5. Program sudah siap dimainkan

**PETUNJUK MEMAINKAN APLIKASI "MISTERY GAME"**

1. sesudah run program, maka akan muncul sebuah tulisan "MISTERY GAME", tunggu beberapa saat

2. setelah "MISTERY GAME" hilang dari layar, akan muncul tulisan "GAME START", yang menandakan permainan akan dimulai

3. setelah "GAME START" menghilang, akan muncul "TIME REMAINING :" dan waktu yang tersisa, yang menandakan permainan telah dimulai

4. cari kombinasi keypad yang dapat menyalakan semua LED, jika anda dapat menemukan tombol yang tepat sebelum waktu habis, maka akan muncul tulisan "CONGRATULATION". Jika waktu habis sebelum anda menemukan kombinasi keypad yang tepat, maka akan muncul tulisan "-TIME OUT-", yang menandakan anda sudah kalah

5. anda dapat menekan tombol reset kapanpun untuk mengulang game. Tombol continue dapat anda tekan untuk mengulang permainan,tetapi dengan pola yang berbeda. Tombol continue hanya bisa ditekan jika anda sudah memenangkan permainan.